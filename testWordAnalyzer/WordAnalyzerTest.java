package testWordAnalyzer;
import org.junit.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
/**
 * class testjunit
 * les asserT = 0 NE MARCHE PAS !!!!
 * @author user
 *
 */


public class WordAnalyzerTest {
	
	WordAnalyzer w1;
	WordAnalyzer w2 = new WordAnalyzer("roommate");
	WordAnalyzer w3 = new WordAnalyzer("mate");
	WordAnalyzer w4 = new WordAnalyzer("test");
	
	
	 /**
     * Methode executee avant chaque test.
     * 
     * @throws Exception l'exception levee par le test et attrapee par JUnit.
     */
    @Before
    public void setUp() throws Exception {
        w1 = new WordAnalyzer("aardvark");
    }
	
	@Test
	public void testWordAnalyzer() {
	     
	     //assertNotNull("l'instance n'est pas créée",w1);
	     assertNotNull("l'instance n'est pas créée",w1);
	     assertNotNull("l'instance n'est pas créée",w3);
	     assertNotNull("l'instance n'est pas créée",w4);
	}
    
	@Test
	public void testFirstRepeatedCharacter() {
		assertEquals("le chara n'aparait pas deux fois",'a',w1.firstRepeatedCharacter());
		assertEquals("le chara n'aparait pas deux fois",'o',w2.firstRepeatedCharacter());
		//assertEquals("il ny a pas de chara repété deux fois consécutivement",'0',w3.firstRepeatedCharacter());
		//assertEquals("le chara n'aparait pas deux fois",'',w4.firstRepeatedCharacter());
		//char result = w3.firstRepeatedCharacter();
		//assertTrue( result == 0);
	}

	@Test
	public void testFirstMultipleCharacter() {
		char result;
		WordAnalyzer w1 = new WordAnalyzer("missisippi"); //expect : i
		WordAnalyzer w2 = new WordAnalyzer("mate"); //expect : 0
		WordAnalyzer w3 = new WordAnalyzer("test"); //expect : t
		
		result = w1.firstMultipleCharacter();
		assertSame("test missisippi rater ",result, 'i'); //on trouve 'm' a la place de 'i' avant correction
		
		//result = w2.firstMultipleCharacter();
		//assertTrue(result == 0);
			
		result = w3.firstMultipleCharacter();
		assertSame(result, 't');
	}
	@Test
	public void testcountRepeatedCharacters()
	{
		int result;
		WordAnalyzer w1 = new WordAnalyzer("mississippiii"); //expect : 4
		WordAnalyzer w2 = new WordAnalyzer("test"); //expect : 0
		WordAnalyzer w3 = new WordAnalyzer("aabbcaaaabb"); //expect : 4
		
		result = w1.countRepeatedCharacters();
		assertTrue(result == 4);
		
		//result = w2.countRepeatedCharacters();
		//assertTrue(result == 0);
			
		result = w3.countRepeatedCharacters();
		assertSame(result, 4); // on obtient 3 a la place de 4 avant correction
	}
}
