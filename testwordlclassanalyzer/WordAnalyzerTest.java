package testwordlclassanalyzer;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
public class WordAnalyzerTest {
	
	WordAnalyzer w1;
	WordAnalyzer w2 = new WordAnalyzer("roommate");
	WordAnalyzer w3 = new WordAnalyzer("mate");
	WordAnalyzer w4 = new WordAnalyzer("test");
	
	
	 /**
     * Methode executee avant chaque test.
     * 
     * @throws Exception l'exception levee par le test et attrapee par JUnit.
     */
    @Before
    public void setUp() throws Exception {
        w1 = new WordAnalyzer();
    }
	
	@Test
	public void testWordAnalyzer() {
	     
	     //assertNotNull("l'instance n'est pas créée",w1);
	     assertNotNull("l'instance n'est pas créée",w1);
	     assertNotNull("l'instance n'est pas créée",w3);
	     assertNotNull("l'instance n'est pas créée",w4);
	}
    
	@Test
	public void testFirstRepeatedCharacter() {
		assertEquals("le chara n'aparait pas deux fois",'a',w1.firstRepeatedCharacter());
		assertEquals("le chara n'aparait pas deux fois",'o',w2.firstRepeatedCharacter());
		//assertEquals("il ny a pas de chara repété deux fois consécutivement",'0',w3.firstRepeatedCharacter());
		//assertEquals("le chara n'aparait pas deux fois",'',w4.firstRepeatedCharacter());
		
	}

	@Test
	public void testFirstMultipleCharacter() {
	      WordAnalyzer w5 = new WordAnalyzer("missisippi");
	      WordAnalyzer w6 = new WordAnalyzer("mate");
	      WordAnalyzer w7 = new WordAnalyzer("test");
	  
	  
	}
/*
	@Test
	public void testCountRepeatedCharacters() {
		fail("Not yet implemented");
	}
*/
}
